export class DateCellRenderer {
    constructor() { }
    val;
    // init method gets the details of the cell to be renderer
    init(params) {
        this.val = document.createElement('span');
        this.val.innerHTML = new Date(params.value).toLocaleDateString();
    }

    getGui() {
        return this.val;
    }
}