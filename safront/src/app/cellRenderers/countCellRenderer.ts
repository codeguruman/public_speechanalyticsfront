export class CountCellRenderer {
    constructor() { }
    val;
    // init method gets the details of the cell to be renderer
    init(params) {
        this.val = document.createElement('span');
        if (params.value != null) {
            this.val.innerHTML = params.value;
        }
    }

    getGui() {
        return this.val;
    }
}