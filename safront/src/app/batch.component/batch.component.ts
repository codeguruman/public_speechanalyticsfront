import { Component } from '@angular/core';
import { HttpService } from '../http.service';
import { State, StateView, MoodMetrics, BadWordsMetrics } from '../processingTaskState';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MoodCellRenderer } from '../cellRenderers/moodCellRenderer'
import { DateCellRenderer } from '../cellRenderers/dateCellRenderer'
import { NameCellRenderer } from '../cellRenderers/nameCellRenderer'
import { CountCellRenderer} from '../cellRenderers/countCellRenderer'

@Component({
    selector: 'batch',
    styleUrls: ['../calls.component.scss'],
    providers: [HttpService],
    templateUrl: './batch.component.html'
})

export class BatchComponent {
    constructor(private httpService: HttpService, private activateRoute: ActivatedRoute,
        private location: Location, private router: Router) {
        let param = activateRoute.snapshot.params['apikey'];
        if (param == null) {
            if (localStorage.hasOwnProperty("token")) {
                if (localStorage["token"].length <= 9) {
                    this.apikey = "demo";
                } else {
                    this.apikey = localStorage["token"];
                }
            } else {
                this.apikey = "demo";
            }
        } else {
            this.apikey = param;
            localStorage.setItem('token', this.apikey);
        }
    }
    stateViews: StateView[];
    states: State[];
    apikey: string;
    private gridApi;

    columnDefs = [
        {
            field: 'num', headerName: 'Номер', minWidth: 100,
            sortable: true
        },
        {
            field: 'fileName', headerName: 'Имя файла', minWidth: 400,
            sortable: true, filter: true, cellRenderer: NameCellRenderer
        },
        {
            field: 'createDate', headerName: 'Дата создания', minWidth: 200,
            maxWidth: 200, sortable: true, filter: true, cellRenderer: DateCellRenderer
        },
        {
            field: 'recognizedText', headerName: 'Распознанный текст', sortable: true, filter: true, minWidth: 300
        },
        {
            field: 'avgMoodLevel', headerName: 'Cредний уровень позитива', minWidth: 300,
            sortable: true, cellRenderer: MoodCellRenderer
        },
        {
            field: 'countBlocksMoodLowerThreshold', headerName: 'Кол-во конфликтов по порогу', minWidth: 340,
            sortable: true, cellRenderer: CountCellRenderer
        },
        {
            field: 'countAnomalySigmaRule', headerName: 'Кол-во конфликтов по разбросу', minWidth: 340,
            sortable: true, cellRenderer: CountCellRenderer
        },
        {
            field: 'badPhrasesCount', headerName: 'Кол-во конфликтных фраз', minWidth: 300,
            sortable: true, cellRenderer: CountCellRenderer
        },
        {
            field: 'notificationSent', headerName: 'Нотификация', minWidth: 300,
            sortable: true
        },
        /* {
            field: 'badPhrases', headerName: 'Проблемные фразы', minWidth: 300,
            filter: true
        }, */
        {
            field: 'taskId', headerName: 'Id', minWidth: 120,
            maxWidth: 120, sortable: true, filter: true, hide: true
        },
        { field: 'error', headerName: 'Ошибки', sortable: true, filter: true, hide: true }
    ];
    rowData;

    prepareStateViews() {
        this.stateViews = [];
        let idx = 1;
        for (let item of this.states) {
            let stateView = new StateView();
            stateView.fileName = item["fileName"];
            stateView.recognizedText = item["recognizedText"];
            stateView.taskId = item["taskId"];
            stateView.error = item["error"];
            stateView.createDate = item["createDate"];
            stateView.avgMoodLevel = item["moodMetrics"]["avgMoodLevel"];
            stateView.countBlocksMoodLowerThreshold = item["moodMetrics"]["countBlocksMoodLowerThreshold"];
            stateView.countAnomalySigmaRule =  item["moodMetrics"]["countAnomalySigmaRule"];
            stateView.badPhrasesCount = item["badWordsMetrics"]["count"];
            if (Boolean(item["notificationSent"])) {
                stateView.notificationSent = "Отправлена"
            }
            stateView.num = idx;
            //stateView.badPhrases = item["badWordsMetrics"]["phrases"].join("\n");
            this.stateViews.push(stateView);
            idx++;
        }
    }

    async getStates() {
        this.states = await this.httpService.getStates();
        this.prepareStateViews();
        this.rowData = this.stateViews;
    }

    onSelectionChanged() {
        let selectedRows = this.gridApi.getSelectedRows();
        this.openFile(selectedRows[0].fileName, selectedRows[0].taskId);
    }

    openFile(filename, taskId) {
        let link = this.httpService.getLink(filename);
        this.goToTaskResult(taskId, link);
    }

    goToTaskResult(taskId: string, audiolink: string) {
        if (taskId != null) {
            this.router.navigateByUrl('/calls/' + taskId + '?audiolink=' + encodeURIComponent(audiolink));
        }
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridApi.sizeColumnsToFit();
    }

    ngOnInit(): void {
        localStorage.setItem('token', this.apikey);
        this.getStates();
    }
}