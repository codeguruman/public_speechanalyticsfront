import { Component } from '@angular/core';

@Component({
    selector: 'main-app',
    template: `<div>
                    <router-outlet></router-outlet>
               </div>`
})
export class AppComponent { }