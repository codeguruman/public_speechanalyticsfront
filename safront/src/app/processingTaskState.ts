export class ProcessingStates {
    States: State[];
  }

export class State {
    FileName: String;
    TaskId: String;
    CreateDate: String;
    Error: String;
    RecognizedText: String;
    MoodMetrics: MoodMetrics;
  BadWordsMetrics: BadWordsMetrics;
}

export class StateView {
  num: Number;
  fileName: String;
  taskId: String;
  createDate: String;
  error: String;
  recognizedText: String;
  avgMoodLevel: Number;
  countBlocksMoodLowerThreshold: Number;
  countAnomalySigmaRule: Number;
  badPhrasesCount: Number;
  badPhrases: String;
  notificationSent: String;
}

export class MoodMetrics {
  AvgMoodLevel: Number;
  CountBlocksMoodLowerThreshold: Number;
  CountAnomalySigmaRule: Number;
}

export class BadWordsMetrics {
  Count: Number;
  Phrases: String[];
}